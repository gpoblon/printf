/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nblen_base_llu.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/09 11:36:51 by gpoblon           #+#    #+#             */
/*   Updated: 2016/12/09 12:02:38 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_nblen_base_llu(unsigned long long nb, int base)
{
	size_t	len;

	len = 0;
	if (nb == 0)
		++len;
	while (nb)
	{
		nb /= base;
		++len;
	}
	return (len);
}
