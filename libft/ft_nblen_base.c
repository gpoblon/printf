/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nblen_base.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/09 11:38:59 by gpoblon           #+#    #+#             */
/*   Updated: 2016/12/09 11:39:28 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_nblen_base(long long nb, int base)
{
	size_t	len;

	len = 0;
	if (nb <= 0)
		++len;
	while (nb)
	{
		nb /= base;
		++len;
	}
	return (len);
}
